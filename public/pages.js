//const TabActiveColor = "#0096FF";
//const TabInactiveColor = "#1b61ad"

const TabActiveColor = "#FFFFFF";
const TabInactiveColor = "#0099CE"// #0096FF after color management

// tabs
var TabGame;
//var TabEngine;
var TabAbout;
var TabContact;

// sections
var gameSection;
//var engineSection;
var aboutSection;
var contactSection;

const selectTab = function(tab) {
    TabGame.style.color = TabInactiveColor;
    //TabEngine.style.color = TabInactiveColor;
    TabAbout.style.color = TabInactiveColor;
    TabContact.style.color = TabInactiveColor;

    TabGame.Section.style.display = 'none';
    //TabEngine.Section.style.display = 'none';
    TabAbout.Section.style.display = 'none';
    TabContact.Section.style.display = 'none';

    tab.style.color = TabActiveColor;

    tab.Section.style.display = 'flex';
}

window.onload = function() {
    TabGame = document.getElementById('TabGame');
    //TabEngine = document.getElementById('TabEngine');
    TabAbout = document.getElementById('TabAbout');
    TabContact = document.getElementById('TabContact');

    gameSection = document.getElementById('GamePage');
    //engineSection = document.getElementById('EnginePage');
    aboutSection = document.getElementById('AboutPage');
    contactSection = document.getElementById('ContactPage');

    gameSection.style.display = 'None';
    //engineSection.style.display = 'None';
    aboutSection.style.display = 'None';
    contactSection.style.display = 'None';

    TabGame.Section = gameSection;
    //TabEngine.Section = engineSection;
    TabAbout.Section = aboutSection;
    TabContact.Section = contactSection;

    document.body.style.display = 'block';

    selectTab(TabGame);
    TabGame.addEventListener('click', () => {selectTab(TabGame)});
    //TabEngine.addEventListener('click', () => {selectTab(TabEngine)});
    TabAbout.addEventListener('click', () => {selectTab(TabAbout)});
    TabContact.addEventListener('click', () => {selectTab(TabContact)});

};
